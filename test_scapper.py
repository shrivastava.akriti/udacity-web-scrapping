import pytest 
import json
from scapper import UdacityScapper


class Test():
    def get_scraper(self):  
        return UdacityScapper()

    def test_get_courses(self):
        '''test for average number of courses available on site each day'''
        courses = self.get_scraper().udacity_courses()   
        total_courses = len(json.loads(courses))  
        assert total_courses > 10

    def test_get_courses_title(self):
        '''test for number of titles available on site'''
        courses = self.get_scraper().udacity_courses() 
        titles = ['Machine Learning Engineer for Microsoft Azure' , 'AI for Healthcare' , 'Intel® Edge AI for IoT Developers' , 'Intro to Machine Learning with TensorFlow' , 'AI Product Manager' , 'Intro to Machine Learning with PyTorch' , 'AI Programming with Python' , 'Artificial Intelligence for Trading' , 'Computer Vision' , 'Natural Language Processing' , 'Deep Reinforcement Learning' , 'Artificial Intelligence' , 'Machine Learning Engineer', 'Deep Learning' , 'AI for Business Leaders' , 'Machine Learning DevOps Engineer' , 'Supervised Learning' , 'Unsupervised Learning']
        courses = json.loads(courses) 
        import pdb;pdb.set_trace()
        # using for loop
        for course in courses:
            assert course.get('title') in titles 